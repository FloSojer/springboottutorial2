package com.dailycodebuffer.Springboot.tutorial.service;

import com.dailycodebuffer.Springboot.tutorial.entity.Department;
import com.dailycodebuffer.Springboot.tutorial.error.DepartmentNotFoundException;

import java.util.List;

public interface DepartmentSerivice {
    public Department saveDepartment(Department department);

    public List<Department> fetchDepartmentList();

     public Department fetchDepartmentById(Long departmentID) throws DepartmentNotFoundException;

   public void deleteDepartmentById(Long departmentID);

   public Department updateDepartment(Long departmentId, Department department);

    Department fechDepartmentByName(String departmentName);
}
