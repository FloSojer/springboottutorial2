package com.dailycodebuffer.Springboot.tutorial.controller;

import com.dailycodebuffer.Springboot.tutorial.entity.Department;
import com.dailycodebuffer.Springboot.tutorial.error.DepartmentNotFoundException;
import com.dailycodebuffer.Springboot.tutorial.service.DepartmentSerivice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class DepartmentController {

    @Autowired
    private DepartmentSerivice departmentSerivice;

    private final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class);

    @PostMapping("/departments")
    public Department saveDepartment(@Valid @RequestBody Department department){
        LOGGER.info("inside saveDepartment of DepartmentController");
        return departmentSerivice.saveDepartment(department);
    }

    @GetMapping("/departments")
    public List<Department> fetchDepartmentList(){
        LOGGER.info("inside fetchDepartmentList of DepartmentController");
        return departmentSerivice.fetchDepartmentList();
    }

    @GetMapping("/departments{id}")
    public Department fetchDepartmentById(@PathVariable("id") Long departmentID) throws DepartmentNotFoundException {
        return departmentSerivice.fetchDepartmentById(departmentID);
    }

    @DeleteMapping("/departments{id}")
    public String deleteDepartmentById( @PathVariable("id") Long departmentID){
        departmentSerivice.deleteDepartmentById(departmentID);
        return "Department Deleted Successfully";
    }

    @PutMapping("/departments{id}")
    public Department updateDepartment(@PathVariable("id") Long departmentId,@RequestBody Department department){
        return departmentSerivice.updateDepartment(departmentId, department);

    }

    @GetMapping("/departsment/name/{name}")
   public Department fechDepartmentByName(@PathVariable("name") String departmentName){
    return departmentSerivice.fechDepartmentByName(departmentName);
   }
}
