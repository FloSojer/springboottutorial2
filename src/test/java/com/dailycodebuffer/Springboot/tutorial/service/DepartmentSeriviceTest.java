package com.dailycodebuffer.Springboot.tutorial.service;

import com.dailycodebuffer.Springboot.tutorial.entity.Department;
import com.dailycodebuffer.Springboot.tutorial.repository.DepartmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoPostProcessor;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DepartmentSeriviceTest {
    @Autowired
    private DepartmentSerivice departmentSerivice;

    @MockBean
    private DepartmentRepository departmentRepository;

    @BeforeEach
    void setUp() {
        Department department =
                Department.builder().
                        departmentName("IT")
                        .departmentAddress("Ahmedabad").
                        departmentCode("IT-06").
                        departmentId(1L).
                        build();
        Mockito.when(departmentRepository.findByDepartmentNameIgnoreCase("IT"))
                .thenReturn(department);
    }
    @Test
    @DisplayName("Get Data based on Valida Department Name")
    public void whenValidDepartmentNameThenDepartmentShoudlFound(){
        String departmentName ="IT";
        Department found = departmentSerivice.fechDepartmentByName(departmentName);

        assertEquals(departmentName, found.getDepartmentName());
    }
}